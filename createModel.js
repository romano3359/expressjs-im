var mongoose = require('./lib/mongoose');
require('./models/menu');
require('./models/slider');
require('./models/catalog');

/*var menu = [
	{name : "Home", url : "/", sort: 100},
	{name : "Men", url : "/catalog/men/", sort: 200},
	{name : "Women", url : "/catalog/women/", sort: 300},
	{name : "Blog", url : "/blog/", sort: 400},
	{name : "Contact", url : "/contact/", sort: 500}
];

menu.forEach(function(item, i, menu) {
	var point = new mongoose.models.Menu(item);
	console.log(point);
	//point.save();
});*/

/*var slider = [
	{name : "Lorem Ipsum is not simply dummy", text : "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor .", button: "Learn more"},
	{name : "There are many variations", text : "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor .", button: "Learn more"},
	{name : "Sed ut perspiciatis unde omnis", text : "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor .", button: "Learn more"}
];

slider.forEach(function(item, i, menu) {
	var point = new mongoose.models.Slider(item);
	//console.log(point);
	point.save();
});*/

var catalog = [
	{
		name : "Мужчинам", 
		text : "", 
		image: "",
		url: "/catalog/men/",
		level: 1,
		is_main: 0,
		id: 1,
		parentid: 0
	},
	{
		name : "Мужчинам", 
		text : "", 
		image: "",
		url: "/catalog/women/",
		level: 1,
		is_main: 0,
		id: 2,
		parentid: 0
	},
	{
		name: "M-Shirts",
		text: "",
		image: "/public/images/pi.jpg",
		url: "/catalog/men/m-shirts/",
		level: 2,
		is_main: 1,
		id: 3,
		parentid: 1
	},
	{
		name: "M-Shoe",
		text: "",
		image: "/public/images/pi1.jpg",
		url: "/catalog/men/m-shoe/",
		level: 2,
		is_main: 1,
		id: 4,
		parentid: 1
	},
	{
		name: "M-Bag",
		text: "",
		image: "/public/images/pi2.jpg",
		url: "/catalog/men/m-bag/",
		level: 2,
		is_main: 1,
		id: 5,
		parentid: 1
	},
	{
		name: "W-Shirts",
		text: "",
		image: "/public/images/pi3.jpg",
		url: "/catalog/women/w-shirts/",
		level: 2,
		is_main: 1,
		id: 6,
		parentid: 2
	},
	{
		name: "W-Shoe",
		text: "",
		image: "/public/images/pi4.jpg",
		url: "/catalog/women/w-shoe/",
		level: 2,
		is_main: 1,
		id: 7,
		parentid: 2
	},
	{
		name: "W-Bag",
		text: "",
		image: "/public/images/pi5.jpg",
		url: "/catalog/women/w-bag/",
		level: 2,
		is_main: 1,
		id: 8,
		parentid: 2
	}
];

catalog.forEach(function(item, i, menu) {
	var point = new mongoose.models.Catalog(item);
	//console.log(point);
	point.save();
});