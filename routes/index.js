/*
 * GET all page.
 */
var Menu = require('../models/menu').Menu;
var Slider = require('../models/slider').Slider;
var Catalog = require('../models/catalog').Catalog;

module.exports = function(app) {
	app.get("/", 
		function(req, res, next) {
			Menu.find({}, function(err, menu) {
				if(err) throw err;

				res.menu = menu;
				next();
			});
		},
		function(req, res, next) {
			Slider.find({}, function(err, slider) {
				if(err) throw err;

				res.slider = slider;
				next();
			});
		},
		function(req, res, next) {
			Catalog.find({is_main: 1}, function(err, catalog) {
				if(err) throw err;

				res.catalog = catalog;
				next();
			});
		},
		function(req, res, next) {
			var meta = {
				title: 'Магазин Демо - Титульник страницы',
				description: 'Магазин Демо - Описание страницы',
				h1: 'Магазин Демо - Заголовок страницы'
			};

			res.render('index', {
				meta: meta,
				menu: res.menu,
				slider: res.slider,
				catalog: res.catalog
			}, function(err, html) {
				if(err) throw err;

				res.send(html);
			})
		}
	);

	app.get("/news", function(req, res, next) {
		res.render('index', {
			title: 'News' 
		})
	});

	app.get("/news/*", function(req, res, next) {
		res.render('index', {
			title: 'News ID' 
		})
	});

	app.get("/reviews", function(req, res, next) {
		res.render('index', {
			title: 'Reviews' 
		})
	});

	app.get("/contact", function(req, res, next) {
		res.render('index', {
			title: 'Contact' 
		})
	});

	app.get("/catalog", function(req, res, next) {
		res.render('index', {
			title: 'Catalog' 
		})
	});

	app.get("/catalog/product-*", function(req, res, next) {
		res.render('index', {
			title: 'Product ID' 
		})
	});

	app.get("/basket", function(req, res, next) {
		res.render('index', {
			title: 'Basket' 
		})
	});

	app.get("/order", function(req, res, next) {
		res.render('index', {
			title: 'Order' 
		})
	});

	app.get("/personal", function(req, res, next) {
		res.render('index', {
			title: 'Personal' 
		})
	});

	app.get("/personal/profile", function(req, res, next) {
		res.render('index', {
			title: 'Personal Profile' 
		})
	});

	app.get("/personal/orderList", function(req, res, next) {
		res.render('index', {
			title: 'Personal Order List' 
		})
	});

	app.get("/admin/reviewList", function(req, res, next) {
		res.render('index', {
			title: 'Admin Review List' 
		})
	});

	app.get("/admin/orderList", function(req, res, next) {
		res.render('index', {
			title: 'Admin Order List' 
		})
	});
}