var mongoose = require('../lib/mongoose'),
	Schema = mongoose.Schema;

var menu = new Schema({
  name: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  sort: {
  	type: Number,
    required: true
  }
});

exports.Menu = mongoose.model('Menu', menu);