var mongoose = require('../lib/mongoose'),
	Schema = mongoose.Schema;

var slider = new Schema({
  name: {
    type: String,
    required: true
  },
  text: {
    type: String
  },
  button: {
  	type: String
  }
});

exports.Slider = mongoose.model('Slider', slider);