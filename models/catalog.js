var mongoose = require('../lib/mongoose'),
	Schema = mongoose.Schema;

var catalog = new Schema({
  name: {
    type: String,
    required: true
  },
  text: {
    type: String
  },
  image: {
  	type: String
  },
  url: {
    type: String
  },
  level: {
    type: Number,
    required: true
  },
  is_main: {
    type: Number
  },
  id: {
    type: Number
  },
  parentid: {
    type: Number
  }
});

exports.Catalog = mongoose.model('Catalog', catalog);